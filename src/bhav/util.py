import csv
import zipfile
import os
import requests
from datetime import datetime, timedelta
from io import TextIOWrapper, StringIO
import pandas as pd

months = {
    '01': {'name':'JAN', 'days': 31},
    '02': {'name':'FEB', 'days': 29},
    '03': {'name':'MAR', 'days': 31},
    '04': {'name':'APR', 'days': 30},
    '05': {'name':'MAY', 'days': 31},
    '06': {'name':'JUN', 'days': 30},
    '07': {'name':'JUL', 'days': 31},
    '08': {'name':'AUG', 'days': 31},
    '09': {'name':'SEP', 'days': 30},
    '10': {'name':'OCT', 'days': 31},
    '11': {'name':'NOV', 'days': 30},
    '12': {'name':'DEC', 'days': 31},
}

def fetch(url_template, date, target_dir, zipped, columns, filters=None):
    url = url_from_template(url_template, date)
    if (zipped):
        target_zip = target_dir+'/'+ date+'.zip'
        if not os.path.exists(target_dir+'/csv'):
            os.mkdir(target_dir+'/csv')
        target_csv = target_dir+'/csv/'+ date+'.csv'
    else:
        if not os.path.exists(target_dir+'/csv'):
            os.mkdir(target_dir+'/csv')
        target_zip = target_dir+'/csv/'+ date+'.csv'
        target_csv = target_zip

    print("fetching from %s into %s " % (url, target_zip))
    headers = {'referer': 'https://www.nseindia.com/all-reports',
               'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.15'}
    try:
        response = requests.get(url, headers=headers, timeout=5)
        response.raise_for_status()

        with open(target_zip, "wb") as file:
            file.write(response.content)
            file.close()

        if zipped:
            data = unzip(target_zip)
        else:
            data = []
            # Try different encodings
            encodings_to_try = ['utf-8', 'latin-1', 'iso-8859-1', 'cp1252']
            success = False

            for encoding in encodings_to_try:
                try:
                    with open(target_csv, 'rb') as file:
                        reader = csv.reader(TextIOWrapper(file, encoding, errors='replace'))
                        data = list(reader)
                        success = True
                        break
                except UnicodeDecodeError:
                    continue
                except Exception as e:
                    print(f"Error reading file with {encoding} encoding: {str(e)}")
                    continue

            if not success:
                raise ValueError(f"Unable to read the CSV file with any of the attempted encodings: {encodings_to_try}")

        if columns.find('DATE') == -1:
            writeCSV(data, target_csv, columns, addDate=True, date=date)
        else:
            writeCSV(data, target_csv, columns)

        if filters is not None:
            filterAndWriteCSV(target_csv, filters, columns)

    except requests.exceptions.HTTPError as errh:
        print("Http Error:", errh)
    except requests.exceptions.ConnectionError as errc:
        print("Error Connecting:", errc)
    except requests.exceptions.Timeout as errt:
        print("Timeout Error:", errt)
    except requests.exceptions.RequestException as err:
        print("Oops: Something Else", err)
    except Exception as e:
        print(f"Unexpected error: {str(e)}")

def unzip(source):
    print("unzipping %s" % (source))
    try:
        with zipfile.ZipFile(source, "r") as zip_ref:
            for file in zip_ref.namelist():
                print("Extracting %s from zip" % file)
                data = []
                # Try different encodings for zip content
                encodings_to_try = ['utf-8', 'latin-1', 'iso-8859-1', 'cp1252']
                success = False

                for encoding in encodings_to_try:
                    try:
                        with zip_ref.open(file) as myfile:
                            reader = csv.reader(TextIOWrapper(myfile, encoding, errors='replace'))
                            data = list(reader)
                            success = True
                            break
                    except UnicodeDecodeError:
                        continue
                    except Exception as e:
                        print(f"Error reading zip content with {encoding} encoding: {str(e)}")
                        continue

                if not success:
                    raise ValueError(f"Unable to read the ZIP content with any of the attempted encodings: {encodings_to_try}")

                return data
    except Exception as e:
        print(f"Error unzipping file: {str(e)}")
        raise

def writeCSV(data, target, header, addDate=False, date=None):
    linecount = 0
    if addDate:
        if date is None:
            raise ValueError("date parameter is required when addDate is True")
        header = 'DATE,'+ header

    f = StringIO(header)
    reader = csv.reader(f, delimiter=',')
    header_row = next(reader, None)
    if header_row is None:
        raise ValueError("Could not parse header row")

    with open(target, "w") as csv_file:
        csv_writer = csv.writer(
            csv_file, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL, lineterminator='\n'
        )
        for row in data:
            # print(row, flush=True)
            if linecount == 0:
                 print('Incoming headers',row)
                 print('Setting headers',header_row)
                 csv_writer.writerow(header_row)
                 linecount+=1
            else:
                if addDate and date is not None:
                    row.insert(0, date[6:] + '/'+ date[4:6]+'/' + date[:4])
                csv_writer.writerow(row)
                linecount+=1
        print("csv had %d lines" % linecount)
        csv_file.close()


def filterAndWriteCSV(source, filters, headers):
    print(source)
    df = pd.read_csv(source, sep=",") #change file to filepath
    # print(df)
    filtered = df.query(filters)
    # print(filtered)
    filtered.to_csv(source, sep=',',index=False)

def today():
    return datetime.today().strftime('%Y%m%d')

def url_from_template(template, date):
    template = template.replace('#-#day#-#', date[6:])
    template = template.replace('#-#month#-#', date[4:6])
    template = template.replace('#n#month#n#', months[date[4:6]]['name'])
    template = template.replace('#-#year#-#', date[:4])
    template = template.replace('#-#YY#-#', date[2:4])
    return template

def dateList(start, end):
    start = datetime.strptime(start, "%Y%m%d")
    end = datetime.strptime(end, "%Y%m%d")
    date_generated = [start + timedelta(days=x) for x in range(0, (end-start).days + 1)]
    dates = [x.strftime('%Y%m%d') for x in date_generated]

    return dates
